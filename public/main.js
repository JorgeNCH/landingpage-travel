/* Tabs */
function Tabs() {
    
    this.globalTabs = document.querySelectorAll('.c-tabs');
    this.before = 0;
    this.content = null;

    selfTab = this;

    this.passItems = function(element) {
        let tabs = element.querySelectorAll('.tabs ul li a')
        selfTab = selfTab

        tabs.forEach(function(item, i) {
            selfTab = selfTab


            if (item.classList.contains('selected')) {
                selfTab.content = element.querySelector('.tab-content[data-sel="' + item.innerHTML + '"]')
            }


            item.addEventListener('click', function() { console.log(selfTab)
                tabs[selfTab.before].classList.remove('selected')
                item.classList.add('selected')
                selfTab.before = i

                if (selfTab.content) {
                    selfTab.content.classList.replace('selected','deselected')
                } 

                window.setTimeout(function() {
                    selfTab.content.classList.remove('deselected')
                    selfTab.content = element.querySelector('.tab-content[data-sel="' + item.innerHTML + '"]')
                    selfTab.content.classList.add('selected')
                }, 300)

            }, false)
        });
    }


    this.globalTabs.forEach(function(element) {
        
        selfTab.passItems(element)


    });
    
    //return this;
}


function playCover() {

    this.control = document.getElementById('videoControl');

    this.play = this.control.querySelector('.svgPlay');
    this.pause = this.control.querySelector('.svgPause');
    this.progress = this.control.querySelector('.svgProgress');


    this.video = document.getElementById('coverVideo');

    console.log(this.video.readyState)

    if (this.video.paused) {
        this.pause.style.display = 'none';
        this.play.style.display = 'block';
    } else {
        this.play.style.display = 'none';
        this.pause.style.display = 'block';
    }

    let self = this


    this.control.addEventListener('click', function() {

        if (self.video.paused) {
            self.play.style.display = 'none';
            self.pause.style.display = 'block';
            self.video.play()
        } else {
            self.pause.style.display = 'none';
            self.play.style.display = 'block';
            self.video.pause()
        }   

    }, false)



    this.video.addEventListener('timeupdate', function() {

        //console.log(self.video.currentTime, self.video.duration)

        let r = self.progress.getAttribute('r')
        let c = Math.PI * (r*2)

        let procent = Math.floor((self.video.currentTime * 100 ) / self.video.duration)
        
        let pct = ((100 - procent)/100) * c


        self.progress.style.strokeDashoffset = pct


    });

}

/* Slider */

function Slider() {

    this.carrusel = document.querySelector('.carrusel');
    this.items = this.carrusel.querySelectorAll('.slider-item');

    this.current = 0;
    this.inwork;


    this.top = this.items.length - 1;
    console.log('top',this.top)
    self = this;
    
    this.items.forEach(function(item,i) {

        if (item.classList.contains('selected')) {
            self.current = i
        }
    })

    if (this.current == 0) {
        this.items[this.top].classList.add('prev')
        this.items[this.current].classList.add('selected')
        this.items[this.current + 1].classList.add('next')
    } else {
        this.items[(this.current - 1) < 0 ? this.top: this.current - 1].classList.add('prev')
        this.items[this.current].classList.add('selected')
        this.items[(this.current + 1) > this.top ? 0: this.current + 1].classList.add('next')
    }




    this.prev = function() {
        
        console.log(self.current, self.items);
        
        window.clearTimeout(self.inwork)

        self.current -= 1

        if (self.current < 0) {
            self.current = self.top
        }
        self.innerTimeout()
    }


    this.next = function() {
        
        window.clearTimeout(self.inwork)
        
        console.log(self.top, self.current);
        
        self.current += 1
        if (self.current > self.top) {
            self.current = 0
        }
        
        self.innerTimeout()
    }

    this.innerTimeout = function () {
        self.carrusel.classList.add('out')
        self.inwork = window.setTimeout(self.addEvents,350)
    }

    this.addEvents = function () {
        self.carrusel.classList.remove('out')

        let prev = (self.current - 1) < 0 ? self.top: self.current - 1;
        let next = (self.current + 1) > self.top ? 0: self.current + 1;


        self.items.forEach(function(item, i) {
            item.removeEventListener('click', self.next)
            item.removeEventListener('click', self.prev)

            if (i != prev && i != next && i != self.current) {
                item.className = 'slider-item'
            }


        })


        self.items[next].addEventListener('click', self.next);
        self.items[next].className = 'slider-item next'
        self.items[self.current].className = 'slider-item selected'
        self.items[prev].addEventListener('click', self.prev);
        self.items[prev].className = 'slider-item prev'
    }

    this.addEvents();

}


imagesLoaded(document.querySelectorAll('img'), function(i) {
    console.log('All images is loaded', i)
    document.body.classList.remove('loading')
})


window.onload = function() {
    tabs = new Tabs();
    new playCover();
    new Slider();

    let allImages = document.querySelectorAll('img');
    allImages.forEach(function(ima) {
        if (ima.getAttribute('data-src')) {
            ima.setAttribute('src', ima.getAttribute('data-src'))
        }
    })


    let menuView = document.getElementById('mainMenu')

    document.getElementById('btnMenu').addEventListener('click', function() {

        menuView.classList.toggle('open');

    })

    document.querySelector('.floatMenu').addEventListener('click', function() {
        menuView.classList.toggle('open');
    })

}